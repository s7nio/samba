## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/hello-world/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/hello-world/commits/master)

## hello-world
Bastille template to bootstrap hello-world

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/hello-world
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/hello-world
```
